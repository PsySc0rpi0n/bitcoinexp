#include <stdio.h>
#include <gmp.h>

#include "../inc/comp_pubkey.h"

int compress_pubkey(gmp_coord raw_pbk, mpz_t* comp_pbk, mpz_t* uncomp_pbk) {
    size_t sizein16 = mpz_sizeinbase(raw_pbk.x, 16);
    mp_bitcnt_t exp2 = (mp_bitcnt_t) sizein16 << 2;
    mpz_t prefix_z;


    // check if Y coordinate of raw pub key is odd or even
    if(mpz_tstbit(raw_pbk.y, 0))
        // prefix 03 for odd coord
        mpz_init_set_ui(prefix_z, 3);
    else
        // prefix 02 for even coord
        mpz_init_set_ui(prefix_z, 2);

    // Add the prefix to the X coordinate of pub key
    mpz_mul_2exp(prefix_z, prefix_z, exp2);
    mpz_add(*comp_pbk, raw_pbk.x, prefix_z);
    
    // Concatenate the 2 coordinates of the raw pub key
    mpz_mul_2exp(*uncomp_pbk, raw_pbk.x, exp2);
    mpz_add(*uncomp_pbk, *uncomp_pbk, raw_pbk.y);

    // Add 04 prefix to the uncompressed pub key
    sizein16 = mpz_sizeinbase(*uncomp_pbk, 16);
    exp2 = (mp_bitcnt_t) sizein16 << 2;
    mpz_set_ui(prefix_z, 4);
    mpz_mul_2exp(prefix_z, prefix_z, exp2);
    mpz_add(*uncomp_pbk, *uncomp_pbk, prefix_z);

    mpz_clears(prefix_z, NULL);
    return 0;
}
