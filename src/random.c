#include <math.h>
#include <stdlib.h>

#include "../inc/random.h"

#define KEYSIZE 32

int gen_priv_key(mpz_t* pkey, mpz_t n) {
    FILE* fpointer = NULL;
    unsigned char* data = NULL;
    mpz_t imp_val, tmp;

    mpz_inits(imp_val, tmp, NULL);
    mpz_sub_ui(tmp, n, 1); // tmp = n - 1 for the key generation between 0 and n - 1
    if( (data = (unsigned char*) malloc(KEYSIZE)) == NULL) {
        // printf("Memory error!\n");
        return -1;
    }

    if( (fpointer = fopen("/dev/urandom", "rb")) == NULL )
        return -2;

    do{
        fread(data, KEYSIZE, 1, fpointer);
        mpz_import(imp_val, 1, 1, KEYSIZE, 1, 0, data);
    }while(mpz_cmp(imp_val, tmp) > 0);
    // gmp_printf("0x%Zx\n", imp_val);
    mpz_set(*pkey, imp_val);
    mpz_clears(imp_val, tmp, NULL);
    free(data);
    fclose(fpointer);
    return 0;
}
