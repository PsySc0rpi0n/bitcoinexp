#include <stdio.h>
#include <gmp.h>
#include <stdlib.h>
#include <inttypes.h>

#include "../inc/ecdsa_arithmetic.h"

void fixed_inverse(mpz_t* res, mpz_t a, mpz_t p) {
    if (mpz_cmp_ui(a, 0) == 0) {
        mpz_set_str(*res, "1", 0);
        return;
    }

    mpz_invert(*res, a, p);
}

void ecdsa_double(gmp_coord point, gmp_coord* pointDouble, mpz_t opa, mpz_t opP){
    mpz_t opPow, opSum, opMult, opInv, opSlope, opSub;
    mpz_t tmp, tmp1, tmp2;
    mpz_inits(opPow, opSum, opMult, opInv, opSlope, opSub, NULL);
    mpz_inits(tmp, tmp1, tmp2, NULL);

    // Compute the operations needed in the numerator of (3X² + a) / (2Y)
    mpz_pow_ui(opPow, point.x, 2);              // X²
    mpz_set_ui(tmp, 3);
    mpz_mul(opMult, opPow, tmp);                // 3X²
    mpz_add(opSum, opMult, opa);                // 3X² + a

    // Compute the operations needed in the denominator of (3X² + a) / (2Y)
    mpz_mul_ui(tmp1, point.y, 2);               // 2Y
    fixed_inverse(&opInv, tmp1, opP);           // 1/ 2Y
    mpz_mul(tmp2, opSum, opInv);                // (3X² + a) * ( 1 / (2Y) )

    // Compute the operation ((3X² + a) * (1 / (2Y))) % p
    mpz_mod(opSlope, tmp2, opP);
    
    // Compute the new X coord of the doubled point
    mpz_inits(opPow, opMult, NULL);
    mpz_pow_ui(opPow, opSlope, 2);              // opSlope²   
    mpz_mul_ui(opMult, point.x, 2);             // 2X
    mpz_sub(opSub, opPow, opMult);              // opSlope² - 2x
    mpz_mod(pointDouble->x, opSub, opP);        // (opSlope² - 2x) % p
        
    // Compute the new Y coord of the doubled point
    mpz_inits(opSub, opMult, tmp, NULL);
    mpz_sub(opSub, point.x, pointDouble->x);    // (x - X)
    mpz_mul(opMult, opSlope, opSub);            // opSlope(x - X)
    mpz_sub(tmp, opMult, point.y);              // opSlope(x - X) - y
    mpz_mod(pointDouble->y, tmp, opP);          // (opSlope(x - X) - y) % p

    mpz_clears(opPow, opSum, opMult, opInv, opSlope, opSub, NULL);
    mpz_clears(tmp, tmp1, tmp2, NULL);
}

void ecdsa_add(gmp_coord pointA, gmp_coord pointB, gmp_coord* pointAdd,
                                                mpz_t opA, mpz_t opP){

    mpz_t opSlope, opSub, opSub1, opInv, opPow, opMult;
    mpz_inits(opSlope, opSub, opSub1, opInv, opPow, opMult, NULL);

    if(!(mpz_cmp(pointA.x, pointB.x) && mpz_cmp(pointA.y,  pointB.y))){
        ecdsa_double(pointA, pointAdd, opA, opP);
    }else{
        // Computation of opSlope = (y - Y) / (x - X)
        mpz_sub(opSub, pointA.y, pointB.y);     // (y - Y)
        mpz_sub(opSub1, pointA.x, pointB.x);    // (x - X)
        fixed_inverse(&opInv, opSub1, opP);     // 1 / (x - X)
        mpz_mul(opMult, opSub, opInv);          // (y - Y) * (1 / (x - X))
        mpz_mod(opSlope, opMult, opP);          // (y - Y) * (1 / (x - X)) % p

        // Computation of nX = opSlope² - x - X
        mpz_inits(opPow, opSub, opSub1, NULL);
        mpz_pow_ui(opPow, opSlope, 2);          // opSlope²
        mpz_sub(opSub, opPow, pointA.x);        // opSlope² - x
        mpz_sub(opSub1, opSub, pointB.x);       // (opSlope² - x) - X
        mpz_mod(pointAdd->x, opSub1, opP);      // ((opSlope² - x) - X) % p
    
        // Computation of nY = opSlope(x - nX) - Y
        mpz_inits(opSub, opMult, opSub1, NULL);
        mpz_sub(opSub, pointA.x, pointAdd->x);  // (x - nX)
        mpz_mul(opMult, opSlope, opSub);        // opSlope(x - nX)
        mpz_sub(opSub1, opMult, pointA.y);      // opSlope(x - nX) - Y
        mpz_mod(pointAdd->y, opSub1, opP);      // (opSlope(x - nX) - Y) % p
    }
    mpz_clears(opSlope, opSub, opSub1, opInv, opPow, opMult, NULL);
}

void ecdsa_multiply(gmp_coord point, gmp_coord* newPoint, mpz_t pKey, mpz_t opA,
                    mpz_t opP){
    gmp_coord tmp;

    mpz_inits(tmp.x, tmp.y, NULL);
    mpz_set(tmp.x, point.x);
    mpz_set(tmp.y, point.y);

    char* binary = NULL;
    size_t nbits = mpz_sizeinbase(pKey, 2);

    if (!(binary = (char*) malloc(nbits + 1)))
        exit(-1);

    mpz_get_str(binary, 2, pKey);
    // printf("%s\n", binary);
    for(size_t i = 1; i < nbits; i++){
        ecdsa_double(tmp, newPoint, opA, opP);
        mpz_set(tmp.x, newPoint->x);
        mpz_set(tmp.y, newPoint->y);
        if (binary[i] == '1'){
            ecdsa_add(tmp, point, newPoint, opA, opP);
            mpz_set(tmp.x, newPoint->x);
            mpz_set(tmp.y, newPoint->y);
        }
    }
    free(binary);
}
