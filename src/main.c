#include <stdio.h>
#include <gmp.h>
#include <stdlib.h>

#include "../inc/ecdsa_arithmetic.h"
#include "../inc/random.h"
#include "../inc/comp_pubkey.h"

int main(void){
    mpz_t a, b, n, P, privKey, compressed_pubKey, uncompressed_pubKey;
    mpz_t tmp;
    mpz_t tmp1;
    mpz_t tmp2;
    gmp_coord pointG, pointQ;

    mpz_inits(a, b, n, P, privKey, compressed_pubKey, uncompressed_pubKey,
              tmp, tmp1, tmp2, NULL);
    mpz_inits(pointQ.x, pointQ.y, pointG.x, pointG.y, NULL);
    
    mpz_set_str(a, "0x0000000000000000000000000000000000000000000000000000000000000000", 0);
    mpz_set_str(b, "0x0000000000000000000000000000000000000000000000000000000000000007", 0);
    mpz_set_str(n, "0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141", 0);
    mpz_set_str(P, "fffffffffffffffffffffffffffffffffffffffffffffffffffffffefffffc2f", 16);
    mpz_set_str(pointG.x, "0x79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798", 0);
    mpz_set_str(pointG.y, "0x483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8", 0);
    gen_priv_key(&privKey, P);
    
    ecdsa_multiply(pointG, &pointQ, privKey, a, P);
    gmp_printf("PrivKey = %#Zx\n", privKey);
    printf("PubKey Q[x, y] = privKey * [G.x, G.y]\n"); 
    gmp_printf("PubKey ==> Q.x = %#Zx\n", pointQ.x);
    gmp_printf("PubKey ==> Q.y = %#Zx\n", pointQ.y);

    compress_pubkey(pointQ, &compressed_pubKey, &uncompressed_pubKey);
    gmp_printf("Compressed Public Key: %#.66Zx\n", compressed_pubKey);
    gmp_printf("Uncompressed Public Key: %#.130Zx\n", uncompressed_pubKey);

    mpz_clears(a, b, n, P, pointG.x, pointG.y,  pointQ.x, pointQ.y, privKey, tmp, tmp1, tmp2,
                            compressed_pubKey, uncompressed_pubKey, NULL);
    return 0;
    // print *pointa.x[0][0]._mp_d
    // call ((int (*)(const char *, ...))__gmp_printf)("%Zd\n", x)
}
