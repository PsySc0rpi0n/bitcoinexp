GCC := gcc
CFLAGS := -Wall -Werror -Wextra -pedantic -std=c99 -lm -g3#-fsanitize=address,undefined
LIBS := -lgmp

# ----- Directories ----- #
OBJ := obj/
INC := inc/
SRC := src/
DPD := .depends/
BIN := bin/

# ----- SOURCE FILES ----- #
SOURCES := $(wildcard $(SRC)*.c)

# ----- OBJECT FILES ----- #
OBJECTS := $(patsubst $(SRC)%.c, $(OBJ)%.o, $(SOURCES))

# ----- DEPEND FILES ----- #
DEPENDS := $(patsubst $(SRC)%.c, $(DPD)%.d, $(SOURCES))

# ----- TARGET RULES ----- #
$(BIN)btctest: $(OBJECTS) | $(BIN)
	@echo "Linking $@"
	$(CC) -o $@ $^ $(LIBS)

-include $(DEPENDS)

$(OBJ)%.o: $(SRC)%.c | $(OBJ)
	@echo "Compiling $*.c"
	$(CC) $(CFLAGS) -c -o $@ $<

$(DPD)%.d: $(SRC)%.c | $(DPD)
	$(GCC) -MM -MG $< | sed 's!^\(.\+\).o:!$(DPD)\1.d $(OBJ)\1.o:!' > $@

$(DPD) $(BIN) $(OBJ):
	mkdir $@

clean:
	rm -rf $(BIN)*
	rm -rf $(OBJ)*
.PHONY: clean
