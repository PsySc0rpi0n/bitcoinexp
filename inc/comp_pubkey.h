#include <gmp.h>
#include "../inc/ecdsa_arithmetic.h"

int compress_pubkey(gmp_coord raw_pbk, mpz_t* comp_pbk, mpz_t* uncomp_pbk);
