#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <gmp.h>
#include <inttypes.h>
#include <sys/random.h>

int gen_priv_key(mpz_t* priv_key, mpz_t n);
