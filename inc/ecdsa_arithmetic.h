#ifndef ECDSA_ARITHMETIC_H
#define ECDSA_ARITHMETIC_H

#include <gmp.h>

typedef struct{
    mpz_t x;
    mpz_t y;
}gmp_coord;

void fixed_inverse(mpz_t* res, mpz_t a, mpz_t p);
void ecdsa_double(gmp_coord point, gmp_coord* pointDouble, mpz_t opa, mpz_t opP);
void ecdsa_add(gmp_coord pointA, gmp_coord pointB, gmp_coord* pointAdd, mpz_t opA, mpz_t opP);
void ecdsa_multiply(gmp_coord point, gmp_coord* newPoint, mpz_t pKey, mpz_t opA, mpz_t opP);

#endif /* ECDSA_ARITHMETIC_H */
